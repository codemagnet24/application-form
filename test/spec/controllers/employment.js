'use strict';

describe('Controller: EmploymentCtrl', function () {

  // load the controller's module
  beforeEach(module('siteApp'));

  var EmploymentCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    EmploymentCtrl = $controller('EmploymentCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
