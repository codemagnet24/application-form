'use strict';

describe('Controller: PartTimeCtrl', function () {

  // load the controller's module
  beforeEach(module('siteApp'));

  var PartTimeCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    PartTimeCtrl = $controller('PartTimeCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
