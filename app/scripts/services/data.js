'use strict';

/**
 * @ngdoc service
 * @name siteApp.data
 * @description
 * # data
 * Factory in the siteApp.
 */
angular.module('siteApp')
.factory('data', function ($http,toaster) {
    var serviceBase = 'http://172.31.1.132/forms/api/v1/';

        var obj = {};
        obj.toast = function (data) {
            toaster.pop(data.status, '', data.message, 4000, 'trustedHtml');
        };
        obj.get = function (q) {
            return $http.get(serviceBase + q).then(function (results) {
                return results.data;
            });
        };
        obj.post = function (q, object) {
            return $http.post(serviceBase + q, object).then(function (results) {
                return results.data;
            });
        };
        obj.put = function (q, object) {
            return $http.put(serviceBase + q, object).then(function (results) {
                return results.data;
            });
        };
        obj.delete = function (q) {
            return $http.delete(serviceBase + q).then(function (results) {
                return results.data;
            });
        };
 
        return obj;
  });
