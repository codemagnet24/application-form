'use strict';

/**
 * @ngdoc function
 * @name siteApp.controller:CertificationCtrl
 * @description
 * # CertificationCtrl
 * Controller of the siteApp
 */
angular.module('siteApp')
  .controller('CertificationCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
