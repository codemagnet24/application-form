'use strict';

/**
 * @ngdoc function
 * @name siteApp.controller:LeaveCtrl
 * @description
 * # LeaveCtrl
 * Controller of the siteApp
 */
angular.module('siteApp')
  .controller('LeaveCtrl', function ($scope,$rootScope,data) {
    $scope.fields = {
      email:'Email Address',
      name:'Name',
      field_1:'English',
      field_2:'Batch',
      field_3:'Section',
      field_4:'Position',
      field_5:'Undertime/Half Day',
      field_6:'Leave of Absence',
      field_7:'Service Incentive Leave',
      field_8:'Maternity or Paternity Leave',
      field_9:'Solo Parent Leave',
      field_10:'Retraction or Others',
      tfield_1:'Reason',
    }
    $scope.form = {
      form_id: $scope.current_form,
      applicant_name: '',
      field_1: '',
      field_2: '',
      field_3: '',
      field_4: '',
      field_5: '',
      field_6: '',
      field_7: '',
      field_8: '',
      field_9: '',
      field_10: '',
      tfield_1: '',
    }

    $scope.submit = function(){
       console.log($scope.form);
       data.post('form_values', {
           data: $scope.form
         }).then(function (results) {       
         data.toast(results);
         alert('saved');
      });
    }
  });
