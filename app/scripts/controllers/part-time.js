'use strict';

/**
 * @ngdoc function
 * @name siteApp.controller:PartTimeCtrl
 * @description
 * # PartTimeCtrl
 * Controller of the siteApp
 */
angular.module('siteApp')
  .controller('PartTimeCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
