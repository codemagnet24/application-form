'use strict';

/**
 * @ngdoc function
 * @name siteApp.controller:PermissionCtrl
 * @description
 * # PermissionCtrl
 * Controller of the siteApp
 */
angular.module('siteApp')
  .controller('PermissionCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
