'use strict';

/**
 * @ngdoc function
 * @name siteApp.controller:EmploymentCtrl
 * @description
 * # EmploymentCtrl
 * Controller of the siteApp
 */
angular.module('siteApp')
  .controller('EmploymentCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
