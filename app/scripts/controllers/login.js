'use strict';

/**
 * @ngdoc function
 * @name siteApp.controller:LoginCtrl
 * @description
 * # LoginCtrl
 * Controller of the siteApp
 */
angular.module('siteApp')
  .controller('LoginCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
