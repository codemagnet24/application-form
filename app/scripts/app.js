'use strict';

/**
 * @ngdoc overview
 * @name siteApp
 * @description
 * # siteApp
 *
 * Main module of the application.
 */
angular
  .module('siteApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'toaster'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      })
      .when('/leave', {
        templateUrl: 'views/leave.html',
        controller: 'LeaveCtrl'
      })
      .when('/part-time', {
        templateUrl: 'views/part-time.html',
        controller: 'PartTimeCtrl'
      })
      .when('/employment', {
        templateUrl: 'views/employment.html',
        controller: 'EmploymentCtrl'
      })
      .when('/permission', {
        templateUrl: 'views/permission.html',
        controller: 'PermissionCtrl'
      })
      .when('/certification', {
        templateUrl: 'views/certification.html',
        controller: 'CertificationCtrl'
      })
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl'
      })
      .when('/404', {
        templateUrl: '404.html',
      })
      .otherwise({
        redirectTo: '/404'
      });
  }).run(function ($rootScope, $location,data) {
     $rootScope.$on("$routeChangeStart", function (event, next, current) {
       var nextUrl = next.$$route.originalPath;
       switch(nextUrl){
         case "/leave":
           $rootScope.current_form = 1;
         break;
       }
     });

  });
